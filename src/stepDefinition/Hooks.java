package stepDefinition;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import Driver.BrowserDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


public class Hooks {
	
	private static Connection con;
	
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost/it526?autoReconnect=true&useSSL=false","root", "12345");
			return con;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return null;
	}

	
	@Before("@deleteproduct")
	public void deleteProduct () throws SQLException {
			System.out.println("CONNECTION:::::");
			getConnection();
			PreparedStatement ps = con.prepareStatement("delete from product where name='Peach'");
			ps.executeUpdate();
	}
	
	@After("@screenshot")
	public void tearDown(Scenario scenario) {
		if (scenario.isFailed())
		{
		final byte[] screenshot = ((TakesScreenshot) BrowserDriver.driver)
		.getScreenshotAs(OutputType.BYTES);
		scenario.embed(screenshot, "image/png"); //stick it in the report
		}
		BrowserDriver.driver.quit();
	}
}
