package stepDefinition;

import java.util.List;

import CheckoutObjects.Delivery;
import Driver.BrowserDriver;
import org.junit.Assert;
import org.openqa.selenium.By;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Delivery_Steps {
	
	@Then("^User sees delivery page$")
	public void user_sees_deliverypage() throws Throwable {
        Assert.assertEquals("Delivery", BrowserDriver.driver.getTitle());
	}
	
	@When("^User enters delivery info$")
	public void user_enters_delivery_info(List<Delivery>  deliveryinfo) throws Throwable {

		for (Delivery delivery : deliveryinfo) {
			BrowserDriver.driver.findElement(By.id("city")).clear();
			BrowserDriver.driver.findElement(By.id("city")).sendKeys(delivery.city());
			BrowserDriver.driver.findElement(By.id("town")).clear();
			BrowserDriver.driver.findElement(By.id("town")).sendKeys(delivery.town());
			BrowserDriver.driver.findElement(By.xpath(".//div[@class='radio']//label[contains(text(),'"+delivery.cargo()+"')]")).click();
		    Thread.sleep(1000);
			}			
	}
	
	@And("^User click proceed payment$")
	public void user_click_proceedpayment() throws Throwable {
			BrowserDriver.driver.findElement(By.xpath("html/body/a[2]")).click();
		    Thread.sleep(1000);
			}
	
}



