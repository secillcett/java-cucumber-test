package stepDefinition;

import Driver.BrowserDriver;
import Pages.CartPage;
import org.junit.Assert;
import org.openqa.selenium.By;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Cart_Steps {

    CartPage cartPage = new CartPage();

	@Then("^I go to cart$")
	public void go_to_cart() throws Throwable {
		cartPage.visit();
	}

	@Then("^I check if I am at cart page$")
	public void shopping_cart_opens() throws Throwable {
		Assert.assertEquals("Sepetim", BrowserDriver.driver.getTitle());
	}

	@When("^I add a random recommended product to cart$")
	public void add_to_cart() throws Throwable {
		cartPage.addRandomRecomendedProduct();
	}
	
//	@When("^User click empty cart$")
//	public void user_click_emptycart() throws Throwable {
//			BrowserDriver.driver.findElement(By.xpath(".//*[@value='Empty cart']")).click();
//		    Thread.sleep(1000);
//			}
//
//	@Then("^cart is empty$")
//	public void cart_is_empty() throws Throwable {
//
//		boolean present;
//		try {
//			BrowserDriver.driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr[2]"));
//			present = true;
//		} catch (Exception e) {
//		   present = false;
//		}
//		Assert.assertFalse(present);
//	}
//
//	@When("^User click continue shopping$")
//	public void user_click_continueshopping() throws Throwable {
//			BrowserDriver.driver.findElement(By.xpath(".//*[@id='j_idt10']/a[1]")).click();
//		    Thread.sleep(1000);
//			}
//
//	@When("^User click proceed delivery$")
//	public void user_click_proceeddelivery() throws Throwable {
//			BrowserDriver.driver.findElement(By.xpath(".//*[@id='j_idt10']/a[2]")).click();
//		    Thread.sleep(1000);
//			}
	
//	@When("^User tries to open cart without login$")
//	public void user_goestocart_withoutlogin() throws Throwable {
//		    BrowserDriver.loadPage("http://localhost:8080//SecilKaplan_ShoppingCart/user/shoppingcart.xhtml");
//		    Thread.sleep(1000);
//			}
	   
}



