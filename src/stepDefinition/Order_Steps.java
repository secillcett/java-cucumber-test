package stepDefinition;

import Driver.BrowserDriver;
import org.junit.Assert;

import cucumber.api.java.en.Then;


public class Order_Steps {
	
	@Then("^User sees order page$")
	public void user_sees_orderpage() throws Throwable {
        Assert.assertEquals("Order Completed", BrowserDriver.driver.getTitle());
	}
}



