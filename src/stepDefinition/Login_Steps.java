package stepDefinition;

import java.util.List;
import java.util.Map;

import CheckoutObjects.Credentials;
import Driver.BrowserDriver;
import Pages.LoginPage;
import cucumber.api.DataTable;
import org.junit.Assert;
import org.openqa.selenium.By;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Login_Steps {


	LoginPage loginPage = new LoginPage();


	@Given("^I go to login page$")
	public void user_is_on_LogIn_Page() throws Throwable {
		loginPage.visit();
	}
	
	@When("^I enter invalid credentials to login$")
	public void user_enters_invalid_credentials(List<Credentials>  usercredentials) throws Throwable {

		for (Credentials credentials : usercredentials) {
			BrowserDriver.driver.findElement(By.id("email")).clear();
			BrowserDriver.driver.findElement(By.id("email")).sendKeys(credentials.getEmail());
			BrowserDriver.driver.findElement(By.id("password")).clear();
			BrowserDriver.driver.findElement(By.id("password")).sendKeys(credentials.getPassword());
		    Thread.sleep(1000);
			BrowserDriver.driver.findElement(By.xpath("//form[@id='form-login']//button[@type='submit']")).click();
		    Thread.sleep(1000);
			}			
	}

	@Then("^I see wrong credentials warning$")
	public void user_sees_warning() throws Throwable {
        Assert.assertEquals("E-posta adresiniz ya da şifreniz yanlış.", BrowserDriver.driver
				.findElement(By.xpath("/html//div[@id='error-message-container']//div[@class='response-message-content']")).getText());
	}

	@When("^I enter valid credentials to login$")
	public void user_enters_valid_credentials(DataTable table) throws Throwable {
		List<Map<String, String>> list = table.asMaps(String.class, String.class);
		loginPage.login(list.get(0).get("email"),list.get(0).get("password"));

	}

}
