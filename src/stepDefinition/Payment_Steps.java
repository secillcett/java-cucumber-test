package stepDefinition;

import java.util.List;

import CheckoutObjects.Payment;
import Driver.BrowserDriver;
import org.junit.Assert;
import org.openqa.selenium.By;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Payment_Steps {
	
	@Then("^User sees payment page$")
	public void user_sees_paymentpage() throws Throwable {
        Assert.assertEquals("Payment", BrowserDriver.driver.getTitle());
	}
	
	@When("^User enters payment info$")
	public void user_enters_delivery_info(List<Payment>  paymentinfo) throws Throwable {

		for (Payment payment : paymentinfo) {
			BrowserDriver.driver.findElement(By.id("cardno")).clear();
			BrowserDriver.driver.findElement(By.id("cardno")).sendKeys(payment.cardno());
			BrowserDriver.driver.findElement(By.id("expdate")).clear();
			BrowserDriver.driver.findElement(By.id("expdate")).sendKeys(payment.expiredate());
		    Thread.sleep(1000);
			}			
	}
	
	@And("^User click save order$")
	public void user_click_saveorder() throws Throwable {
			BrowserDriver.driver.findElement(By.xpath(".//*[@id='j_idt7']/input[2]")).click();
		    Thread.sleep(1000);
			}
	
}



