package cucumberTest;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "Feature" //path to feature files
		,glue={"stepDefinition"} //path to step definitons
		,dryRun = false // checks if all steps have step definitons, 
						//when this is true it only checks does not run tests
		,monochrome = true //to display console output in much readable way
		//,plugin = {"pretty", "html:target/cucumber-reports"} //to get html test report
		,tags = "@runthiss"//what tags to include(@)/exclude(@~)
		,strict = true // will fail execution if there are undefined or pending steps
		)

//CucumberOptions reference
//https://cucumber.github.io/api/cucumber/jvm/javadoc/cucumber/api/CucumberOptions.html

public class TestRunner {
	
	

}