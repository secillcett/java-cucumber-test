package Driver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;




import java.net.URL;
import java.net.MalformedURLException;

public class BrowserDriver {

	public static WebDriver driver = new ChromeDriver();

    public static void waitForLoad() {
    }

    public void waitForLoad(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(pageLoadCondition);
    }


//	@Before
//	public static WebDriver setUp() throws  MalformedURLException{
////		DesiredCapabilities capability = DesiredCapabilities.chrome();
////		capability.setBrowserName("chrome");
////		capability.setPlatform(Platform.LINUX);
////		ChromeOptions chromeOptions = new ChromeOptions();
////		chromeOptions.addArguments("--headless");
////		driver = new RemoteWebDriver(new URL("http://192.168.55.246:30202/wd/hub"), capability);
//		driver = new ChromeDriver();
//		return driver;
//
//	}

//	@After
//	public static void afterTest() {
//		driver.quit();
//	}



}
