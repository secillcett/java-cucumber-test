package Pages;

import java.util.List;
import java.util.Random;

import Driver.BrowserDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CartPage {
    String baseURL = "https://www.hepsiburada.com/ayagina-gelsin/sepetim";
    By shoppingCart = By.id("CartButton");
    By continueButton = By.xpath("//*[@id=\"short-summary\"]/div[1]/div[2]/button");
    By paymentContainer = By.xpath("//*[@class=\"payment-container\"]");
    By finishShoppingButton = By.xpath("//*[@id=\"frm-save-order\"]/button");
    By addressContanier = By.id("addresses");
    By transferButton = By.xpath("//*[@class=\"accordions\"]/div[3]/a");
    By orderContanier = By.id("order-container");
    List<WebElement> recommendedProducts= BrowserDriver.driver.findElements(By.xpath("//div[@id='recommended-products']/..//button[.='Sepete Ekle']"));
    By eftInfoButton = By.xpath("//*[@class=\"button-container\"]/button[2]");
    By transferOptions = By.xpath("//*[@id=\"payment-type-2\"]/div/ul/li");
    By recommendedProductCards = By.xpath("//*[@id=\"recommended-products\"]/div[1]/ul/div[1]/div/li[2]/li/div/div/span");
    By outOfStockRecomendationButton = By.xpath(".//*[@class=\"box outof-stock-reco\"]/ul/li/div/div[3]/div[2]/button");
    By outOfStockRecomendationContanier = By.xpath("//*[@class=\"box outof-stock-reco\"]");
    By getBankName = By.xpath("//*[@class=\"order-payment-info\"]/div/div[2]/p");
    By getTotalPrice = By.xpath("//*[@id=\"short-summary\"]/div[1]/div[1]/div/span");


    public CartPage visit() {
//        BrowserDriver.driver.findElement(shoppingCart).click();
        BrowserDriver.driver.get(baseURL);
        BrowserDriver.waitForLoad();
        return this;
    }

    public CartPage addRandomRecomendedProduct() {

        Random rand = new Random();
        recommendedProducts.get(rand.nextInt(recommendedProducts.size())).click();
        return this;

    }

}
