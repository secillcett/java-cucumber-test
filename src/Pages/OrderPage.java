package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class OrderPage {

    By trackingNumber = By.xpath("//*[@id=\"content\"]/div/div[2]/div[1]/div/div[1]/strong");
    By totalPrice = By.xpath("//*[@id=\"content\"]/div/div[2]/div[1]/div/div[2]/div[3]/strong");
    By transferPrice = By.xpath("//*[@id=\"content\"]/div/div[2]/div[1]/div/div[2]/div[2]/strong");
    By bankName = By.xpath("//*[@id=\"content\"]/div/div[2]/div[2]/strong");
    By paymentOption = By.xpath("//*[@id=\"content\"]/div/div[2]/div[1]/div/div[2]/div[2]/span");

    public OrderPage isTrackingNumber() {
        return this;
    }


}
