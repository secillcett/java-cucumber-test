package Pages;

import Driver.BrowserDriver;
import org.openqa.selenium.By;

public class LoginPage  {

    String baseURL = "https://www.hepsiburada.com/uyelik/giris";
    private By usernameBy = By.id("email");
    private By passwordBy = By.id("password");
    private By submitButtonBy = By.xpath("//*[@id=\"form-login\"]/div[4]");


    public LoginPage visit() {
        BrowserDriver.driver.get(baseURL);
        return this;
    }

    public LoginPage login(String username, String password) {
        BrowserDriver.driver.findElement(usernameBy).sendKeys(username);
        BrowserDriver.driver.findElement(passwordBy).sendKeys(password);
        BrowserDriver.driver.findElement(submitButtonBy).click();
        BrowserDriver.waitForLoad();
        return this;
    }


}
