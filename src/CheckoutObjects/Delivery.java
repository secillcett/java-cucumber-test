package CheckoutObjects;

public class Delivery {
	
	private String city;
	private String town;
	private String cargo;
	
	public String city() {
        return city;
    }
	public String town() {
        return town;
    }
	
	public String cargo() {
        return cargo;
    }

}
